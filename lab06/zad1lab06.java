package com.company.po_gr3_ostrowskijakub;

public class zad1lab06 {
    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        RachunekBankowy.obliczOdsetki(saver1);
        System.out.println(saver1.saldo);
        RachunekBankowy.obliczOdsetki(saver2);
        System.out.println(saver2.saldo);
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        RachunekBankowy.obliczOdsetki(saver1);
        System.out.println(saver1.saldo);
        RachunekBankowy.obliczOdsetki(saver2);
        System.out.println(saver2.saldo);
    }
}

