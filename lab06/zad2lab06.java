package com.company.po_gr3_ostrowskijakub;


import java.util.ArrayList;
import java.util.Arrays;

class IntegerSet {

    public IntegerSet()
    {
        Arrays.fill(this.set, false);
    }

    boolean[] set = new boolean[100];

    public static void writeSet(boolean[] set)
    {
        for(int i = 0; i<100; i++)
        {
            if(set[i])
            {
                System.out.print(i+1 + ", ");
            }
        }
        System.out.print("\n");
    }

    public static String toString(boolean[] s)
    {
        String wynik = "";
        for(int i = 0; i<100; i++)
        {
            if(s[i])
            {
                wynik += String.valueOf(i+1) + " ";
            }
        }
        return wynik;
    }


    public static boolean[] union(boolean[] s1, boolean[] s2)
    {
        boolean[] us = new boolean[100];
        for(int i = 0; i<100; i++)
        {
            if(!s1[i] && !s2[i])
            {
                us[i] = false;
            } else
            {
                us[i] = true;
            }
        }
        return us;
    }
    public static boolean equals(boolean[] s1, boolean[] s2)
    {
        return Arrays.equals(s1, s2);
    }
    public static boolean[] intersection(boolean[] s1, boolean[] s2)
    {
        boolean[] it = new boolean[100];
        for(int i = 0; i<100; i++)
        {
            if(s1[i] && s2[i])
            {
                it[i] = true;
            } else
            {
                it[i] = false;
            }
        }
        return it;
    }
    public static void insertElement(IntegerSet s, int x)
    {
        s.set[x-1] = true;
    }
    public static void deleteElement(IntegerSet s, int x)
    {
        s.set[x-1] = false;
    }
}

public class zad2lab06
{
    public static void main(String[] args)
    {
        IntegerSet s1 = new IntegerSet();
        IntegerSet s2 = new IntegerSet();
        for(int i = 0; i<4; i++)
        {
            s1.set[i] = true;
        }
        for(int i = 2; i<7; i++)
        {
            s2.set[i] = true;
        }

        IntegerSet.writeSet(s1.set);
        IntegerSet.writeSet(s2.set);
        IntegerSet.writeSet(IntegerSet.union(s1.set, s2.set));
        IntegerSet.writeSet(IntegerSet.intersection(s1.set, s2.set));
        IntegerSet.insertElement(s1, 48);
        IntegerSet.writeSet(s1.set);
        IntegerSet.deleteElement(s1, 4);
        IntegerSet.writeSet(s1.set);
        System.out.println(IntegerSet.toString(s1.set));
        IntegerSet s3 = new IntegerSet();
        s3.set = s1.set.clone();
        System.out.println(String.valueOf(IntegerSet.equals(s1.set, s2.set)));
        System.out.print(String.valueOf(IntegerSet.equals(s1.set, s3.set)));
    }
}
