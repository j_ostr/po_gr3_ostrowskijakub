package com.company.po_gr3_ostrowskijakub;


public class RachunekBankowy {
    static double RocznaStopaProcentowa;
    double saldo;

    public RachunekBankowy(double s)
    {
        this.saldo = s;
    }

    public static void obliczOdsetki(RachunekBankowy kto)
    {
        double odestki = (kto.saldo * RocznaStopaProcentowa) / 12;
        kto.saldo += odestki;
    }

    public static void setRocznaStopaProcentowa(double nowaStopa)
    {
        RocznaStopaProcentowa = nowaStopa;
    }

}
