package com.company;
import java.math.BigInteger;

public class zad4 {
    public static void main(String[] args) {
        BigInteger x = Grain(161);
        System.out.print(x.toString());
    }

    public static BigInteger Grain(int n)
    {
        BigInteger result = new BigInteger("2");
        int potega = n*n;
        result = result.pow(potega);
        result = result.subtract(BigInteger.valueOf(1));

        return result;
    }


}
