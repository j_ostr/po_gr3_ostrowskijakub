package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zad2 {
    public static void main(String[] args) {
        System.out.print(count('c', "check"));
    }

    public static int count(char c, String fileName)
    {
        int result = 0;
        Scanner in = null;
        File file = new File("src/resources/" + fileName + ".txt");
        try {
            in = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while(in.hasNext())
        {
            String x = in.nextLine();
            char[] p = x.toCharArray();
            for(int i = 0; i<p.length;i++)
            {
                if(p[i] == c)
                {
                    result++;
                }
            }
        }
        in.close();
        return result;
    }

}
