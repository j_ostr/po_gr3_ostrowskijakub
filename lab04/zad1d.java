package com.company;
import java.util.Scanner;

public class zad1d {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj słowo: ");
        String x = scanner.nextLine();
        System.out.print("\n" + repeat(x, 3));
    }

    public static String repeat(String str, int n)
    {
        String result = "";
        for(int i = 0; i<n; i++)
        {
            result += str;
        }
        return result;
    }
}