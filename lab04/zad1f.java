package com.company;
public class zad1f {
    public static void main(String[] args) {
        System.out.print(change("SSSSłłowwwo"));
    }

    public static String change(String str)
    {
        char[] chars = str.toCharArray();
        StringBuffer s = new StringBuffer(str.length());

        for(int i = 0; i<chars.length; i++)
        {
            if(Character.isUpperCase(chars[i]))
            {
                s.append(Character.toLowerCase(chars[i]));
            } else
            {
                s.append(Character.toUpperCase(chars[i]));
            }
        }
        return s.toString();
    }
}
