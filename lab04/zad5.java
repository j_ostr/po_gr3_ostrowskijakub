package com.company;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class zad5 {
    public static void main(String[] args) {
        BigDecimal x = capital(2569, 11, 2);
        System.out.print(x);
    }

    public static BigDecimal capital(float k, float p, float n)
    {
        BigDecimal result = new BigDecimal("0");
        result = BigDecimal.valueOf(k);
        for(int i = 0; i<n; i++)
        {
            BigDecimal copy = result.multiply(BigDecimal.valueOf(p));
            copy = copy.divide(BigDecimal.valueOf(100));
            result = result.add(copy);
        }
        BigDecimal resultx = result.setScale(2, RoundingMode.HALF_UP);
        return resultx;
    }
}
