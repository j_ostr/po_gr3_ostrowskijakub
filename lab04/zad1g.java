package com.company;

public class zad1g {
    public static void main(String[] args) {
        System.out.println(nice("9876544321"));
        System.out.println(nice("876544321"));
        System.out.println(nice("76544321"));
    }

    public static String nice(String str)
    {
        char[] chars = str.toCharArray();
        StringBuffer s = new StringBuffer(str.length() + (int) (str.length() / 3));

        for(int i = 0; i<chars.length; i++)
        {
            s.append(chars[i]);
            if((i+1)%3 == str.length()%3 & (i + 1) != str.length())
            {
                s.append("\'");
            }
        }
        return s.toString();
    }
}

