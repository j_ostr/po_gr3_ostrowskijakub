package com.company;

public class zad1b {
    public static void main(String[] args) {
        System.out.println(countSubStr("brrbrrbrrbrrbrr", "brr"));
        System.out.println(countSubStr("brbrbrbrbrbrbrbrbrbrbrbrbrbr", "br"));
    }

    public static int countSubStr(String str, String subStr)
    {
        int result = 0;
        char[] strChar = str.toCharArray();
        char[] subStrChar = subStr.toCharArray();
        for(int i = 0; i<=str.length()-subStr.length(); i++)
        {
            int j;
            if(strChar[i] == subStrChar[0])
            {
                for(j = 0; j<subStr.length(); j++)
                {
                    if(strChar[i+j] != subStrChar[j])
                    {
                        break;
                    }
                    if(j+1 == subStr.length() & strChar[i+j] == subStrChar[j])
                    {
                        result++;
                    }
                }
            }

        }
        return result;
    }

}
