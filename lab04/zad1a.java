package com.company;
import java.util.Scanner;


public class zad1a {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("podaj słowo?");
        String słowo = scan.nextLine();

        System.out.println("podaj literę?");
        char c = scan.next().charAt(0);

        System.out.println(countChar(słowo, c));
    }

    public static int countChar(String str,char c) {

        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }
}

