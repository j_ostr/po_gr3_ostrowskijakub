package com.company;
public class zad1h {
    public static void main(String[] args) {
        System.out.println(nice("9876544321", 3));
        System.out.println(nice("876544321", 4));
        System.out.println(nice("76544321", 5));
    }

    public static String nice(String str, int n)
    {
        char[] chars = str.toCharArray();
        StringBuffer s = new StringBuffer(str.length() + (int) (str.length() / n));

        for(int i = 0; i<chars.length; i++)
        {
            s.append(chars[i]);
            if((i+1)%n == str.length()%n & (i + 1) != str.length())
            {
                s.append("\'");
            }
        }
        return s.toString();
    }
}