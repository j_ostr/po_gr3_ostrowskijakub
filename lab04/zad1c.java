package com.company;
import java.util.Scanner;

public class zad1c {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String x = scanner.nextLine();
        System.out.print(middle(x));
    }

    public static String middle(String str)
    {
        String result = "";
        switch(str.length())
        {
            case 1:
            case 2:
                return str;
        }
        int sr = str.length()/2;
        if(str.length()%2 == 0)
        {
            result = str.substring(sr-1, sr+1);
        } else
        {
            result = str.substring(sr, sr+1);
        }
        return result;
    }
}
