package com.company;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zad3 {
    public static void main(String[] args) {
        System.out.print(count("LoremIpsum", "check"));
    }

    public static int count(String str, String fileName)
    {
        int result = 0;
        Scanner in = null;
        File file = new File("src/resources/" + fileName + ".txt");
        try {
            in = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while(in.hasNext())
        {
            String x = in.nextLine();
            String[] p = x.split(" ");
            for(int i = 0; i<p.length;i++)
            {
                if(p[i].equals(str))
                {
                    result++;
                }
            }
        }
        in.close();
        return result;
    }
}
