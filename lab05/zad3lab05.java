package com.company.po_gr3_ostrowskijakub;

import java.util.ArrayList;
import java.util.Collections;

public class zad3lab05 {
    public static void main(String[] args) {
        ArrayList<Integer> a1 = new ArrayList<>(5);
        ArrayList<Integer> a2 = new ArrayList<>(7);
        for(int i = 0; i<5; i++)
        {
            a1.add(i+1);
        }
        for(int i = 0; i<7; i++)
        {
            a2.add(a1.size()+i+1);
        }
        a2.set(2, 14);
        System.out.println(a1);
        System.out.println(a2);
        System.out.println(mergeSorted(a1, a2));
    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> arrAll = new ArrayList<>(a.size() + b.size());
        int n;
        int m;
        if(a.size() < b.size())
        {
            n = b.size();
            m = a.size();
        } else
        {
            n = a.size();
            m = b.size();
        }
        for(int i = 0; i<m; i++)
        {
            arrAll.add(a.get(i));
            arrAll.add(b.get(i));
        }
        if(a.size() > b.size())
        {
            for(int i = m; i<n; i++)
            {
                arrAll.add(a.get(i));
            }
        } else
        {
            for(int i = m; i<n; i++)
            {
                arrAll.add(b.get(i));
            }
        }
        Collections.sort(arrAll);
        return arrAll;
    }
}