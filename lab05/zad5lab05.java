package com.company.po_gr3_ostrowskijakub;

import java.util.ArrayList;

public class zad5lab05 {
    public static void main(String[] args) {
        ArrayList<Integer> a1 = new ArrayList<>(7);
        for(int i = 0; i<7; i++)
        {
            a1.add(i+1);
        }
        System.out.println(a1);
        reverse(a1);
        System.out.println(a1);
    }

    public static void reverse(ArrayList<Integer> a)
    {
        ArrayList<Integer> arrAll = new ArrayList<>(a.size());
        for(int i = a.size()-1; i>=0; i--)
        {
            arrAll.add(a.get(i));
        }
        a = arrAll;
    }
}
