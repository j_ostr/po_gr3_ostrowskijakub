package com.company.po_gr3_ostrowskijakub;

import java.util.ArrayList;

public class zad1lab05 {

    public static void main(String[] args) {
        ArrayList<Integer> a1 = new ArrayList<>(7);
        ArrayList<Integer> a2 = new ArrayList<>(5);
        for(int i = 0; i<7; i++)
        {
            a1.add(i+1);
        }
        for(int i = 0; i<5; i++)
        {
            a2.add(a1.size()+i+1);
        }
        System.out.println(a1);
        System.out.println(a2);
        System.out.println(append(a1, a2));
    }

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> arrAll = new ArrayList<>(a.size() + b.size());
        arrAll.addAll(0, a);
        arrAll.addAll(a.size(), b);


        return arrAll;
    }

}
