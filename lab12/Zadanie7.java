package com.company.po_gr3_ostrowskijakub.lab12;


public class Zadanie7 {

    public static void main(String[] args) {
        int liczba = Integer.parseInt(args[0]);
        sieveOfEratosthenes(liczba);

    }

    public static void sieveOfEratosthenes(int n)
    {
        boolean prime[] = new boolean[n+1];
        for(int i=0;i<n;i++)
            prime[i] = true;

        for(int p = 2; p*p <=n; p++)
        {
            if(prime[p])
            {
                for(int i = p*p; i <= n; i += p)
                    prime[i] = false;
            }
        }

        // Print all prime numbers
        for(int i = 2; i <= n; i++)
        {
            if(prime[i])
                System.out.print(i + " ");
        }
    }


}
