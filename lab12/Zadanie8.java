package com.company.po_gr3_ostrowskijakub.lab12;


import java.util.Stack;

public class Zadanie8 {

    public static void main(String[] args) {
        Stack<String> x = new Stack<>();
        x.add("Test");
        x.add("Test6");
        x.add("Test5");
        x.add("Test4");
        x.add("Test3");
        x.add("Test2");
        print(x);
    }

    public static <T extends Iterable<E>, E> void print(T co)
    {
        for(E i: co)
        {
            System.out.print(i + ",");
        }
    }


}