package com.company.po_gr3_ostrowskijakub.lab12;


import java.util.LinkedList;

public class Zadanie1_2 {

    public static void main(String[] args) {
        LinkedList<String> x = new LinkedList<>();
        x.add("Marian Kowalski");
        x.add("Jarosław Kowalski");
        x.add("Adrian Nowak");
        x.add("Jeremi Kowalski");
        x.add("Jeremi Wiśniowiecki");
        x.add("Andrzej Wiśniowiecki");
        for(String z: x)
        {
            System.out.print(z + "\n");
        }
        System.out.print("\n");
        redukuj(x, 3);
        for(String z: x)
        {
            System.out.print(z + "\n");
        }
        redukujGenerycznie(x, 2);
        System.out.print("\n");
        for(String z: x)
        {
            System.out.print(z + "\n");
        }
    }

    public static void redukuj(LinkedList<String> pracownicy, int n)
    {
        int ileUsuinec = 0;
        for(int i = 0; i<pracownicy.size(); i++)
        {
            if((i+1)%n == 0)
            {
                pracownicy.remove(i-ileUsuinec);
                ileUsuinec++;
            }
        }
    }

    public static <T> void redukujGenerycznie(LinkedList<T> pracownicy, int n)
    {
        int ileUsuinec = 0;
        for(int i = 0; i<pracownicy.size(); i++)
        {
            if((i+1)%n == 0)
            {
                pracownicy.remove(i-ileUsuinec);
                ileUsuinec++;
            }
        }
    }

}
