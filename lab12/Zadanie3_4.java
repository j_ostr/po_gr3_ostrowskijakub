package com.company.po_gr3_ostrowskijakub.lab12;


import java.util.LinkedList;

public class Zadanie3_4 {

    public static void main(String[] args) {
        LinkedList<String> x = new LinkedList<>();
        x.add("Marian Kowalski");
        x.add("Jarosław Kowalski");
        x.add("Adrian Nowak");
        x.add("Jeremi Kowalski");
        x.add("Jeremi Wiśniowiecki");
        x.add("Andrzej Wiśniowiecki");
        for(String z: x)
        {
            System.out.print(z + "\n");
        }
        System.out.print("\n");
        odwroc(x);
        for(String z: x)
        {
            System.out.print(z + "\n");
        }

        System.out.print("\n");
        odwrocGenerycznie(x);
        for(String z: x)
        {
            System.out.print(z + "\n");
        }
    }

    public static void odwroc(LinkedList<String> pracownicy)
    {
        LinkedList<String> nowa = new LinkedList<>();
        LinkedList<String> pracownicyKopia = (LinkedList<String>) pracownicy.clone();
        for(int i = 0; i<pracownicyKopia.size(); i++)
        {
            nowa.add(pracownicy.getLast());
            pracownicy.remove(pracownicy.getLast());
        }
        for(int i = 0; i<nowa.size(); i++)
        {
            pracownicy.add(nowa.get(i));
        }
    }

    public static <T> void odwrocGenerycznie(LinkedList<T> pracownicy)
    {
        LinkedList<T> nowa = new LinkedList<>();
        LinkedList<T> pracownicyKopia = (LinkedList<T>) pracownicy.clone();
        for(int i = 0; i<pracownicyKopia.size(); i++)
        {
            nowa.add(pracownicy.getLast());
            pracownicy.remove(pracownicy.getLast());
        }
        for(int i = 0; i<nowa.size(); i++)
        {
            pracownicy.add(nowa.get(i));
        }
    }

}
