package com.company.po_gr3_ostrowskijakub.lab08;

public class Fortepian extends Instrument {
    public Fortepian(String producent, int rokProdukcji)
    {
        super(producent, rokProdukcji);
    }

    public String dzwiek()
    {
        return "tink, tink, tink";
    }
}
