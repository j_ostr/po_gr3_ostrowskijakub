package com.company.po_gr3_ostrowskijakub.lab08;

import java.time.LocalDate;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new praco("Kowalski", new String[]{"Jan"}, LocalDate.of(1967, 9, 28), "Mężczyzna", 5000, LocalDate.of(2004, 12, 11));
        ludzie[1] = new praco("Nowak", new String[]{"Maria", "Jolanta"}, LocalDate.of(1978, 4, 11), "Kobieta", 3500, LocalDate.of(2009, 11, 5));
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            StringBuilder imiona = new StringBuilder();
            for(String s : p.getImiona())
            {
                imiona.append(s + " ");
            }
            System.out.println(imiona + p.getNazwisko() + ": " + p.getOpis());
        }
    }
}


