package com.company.po_gr3_ostrowskijakub.lab08;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

abstract class Instrument
{
    public Instrument(String producent, int rokProdukcji)
    {
        this.producent = producent;

        DateTimeFormatter format = new DateTimeFormatterBuilder().appendPattern("yyyy").
                parseDefaulting(ChronoField.MONTH_OF_YEAR, 1).parseDefaulting(ChronoField.DAY_OF_MONTH, 1).toFormatter();

        this.rokProdukcji = LocalDate.parse(String.valueOf(rokProdukcji), format);
    }

    public abstract String dzwiek();

    public String getProducent() { return producent; }

    public LocalDate getRokProdukcji() { return rokProdukcji; }

    private final String producent;
    private final LocalDate rokProdukcji;
}