package com.company.po_gr3_ostrowskijakub.lab08;


import java.time.LocalDate;

class praco extends Osoba {
    public praco (String nazwisko, String[] imiona, LocalDate dataUrodzenia, String plec, double pobory, LocalDate dataZatrudnienia) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory() {
        return pobory;
    }

    private LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    public String getOpis() {
        return String.format("pracownik (%s) z pensją %.2f zł zatrudniony od %s", getPlec(), pobory, getDataZatrudnienia().toString());
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}
