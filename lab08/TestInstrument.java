package com.company.po_gr3_ostrowskijakub.lab08;

import java.util.ArrayList;

public class TestInstrument {
    public static void main(String[] args)
    {
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new Flet("Yamaha", 2018));
        orkiestra.add(new Skrzypce("Höfner ", 1997));
        orkiestra.add(new Skrzypce("Yamaha", 2008));
        orkiestra.add(new Fortepian("Steinway & Sons", 1996));
        orkiestra.add(new Flet("Hohner", 2005));

        for(Instrument i: orkiestra)
        {
            System.out.printf("%s - (%s, %s)%n", i.dzwiek(), i.getProducent(), i.getRokProdukcji().toString());
        }
    }
}
