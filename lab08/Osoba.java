package com.company.po_gr3_ostrowskijakub.lab08;

import java.time.LocalDate;

abstract class Osoba {
    public Osoba(String nazwisko, String[] imiona, LocalDate dataUrodzenia, String plec) {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;

        this.plec = plec.toLowerCase().equals("mężczyzna");
    }

    public abstract String getOpis();

    public String getNazwisko() {
        return nazwisko;
    }

    public String[] getImiona() {
        return imiona;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public String getPlec() {
        return plec ? "Mężczyzna" : "Kobieta";
    }

    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;
}
