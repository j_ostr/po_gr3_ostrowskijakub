package com.company.po_gr3_ostrowskijakub.lab08;


public class Skrzypce extends Instrument {
    public Skrzypce(String producent, int rokProdukcji)
    {
        super(producent, rokProdukcji);
    }

    public String dzwiek()
    {
        return "li, li, li";
    }
}
