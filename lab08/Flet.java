package com.company.po_gr3_ostrowskijakub.lab08;


public class Flet extends Instrument {
    public Flet(String producent, int rokProdukcji)
    {
        super(producent, rokProdukcji);
    }

    public String dzwiek()
    {
        return "fi, fi, fi";
    }
}
