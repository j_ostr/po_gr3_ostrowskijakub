package com.company.po_gr3_ostrowskijakub.lab11;


class ArrayAlg {

    public static Pair<Integer> minmax(Integer[] a) {
        if (a == null || a.length == 0) {
            return null;
        }

        Integer min = a[0];
        Integer max = a[0];

        for (int i = 1; i < a.length; i++) {
            if (min.compareTo(a[i]) > 0) {
                min = a[i];
            }

            if (max.compareTo(a[i]) < 0) {
                max = a[i];
            }
        }

        return new Pair<Integer>(min, max);
    }
}