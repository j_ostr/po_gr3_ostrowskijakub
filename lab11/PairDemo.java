package com.company.po_gr3_ostrowskijakub.lab11;

public class PairDemo {

    public static void main(String[] args)
    {
        Integer[] words = { 15, 51, 1, 614, 13, 6643 };
        Pair<Integer> mm = ArrayAlg.minmax(words);
        System.out.println("min = " + mm.getFirst());
        System.out.println("max = " + mm.getSecond());
        mm.swap(mm);
        System.out.println("min = " + mm.getFirst());
        System.out.println("max = " + mm.getSecond());
    }

}