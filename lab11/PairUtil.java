package com.company.po_gr3_ostrowskijakub.lab11;


public class PairUtil {
    public static <T> Pair<T> swap(Pair<T> p)
    {
        return new Pair<T>(p.getSecond(), p.getFirst());
    }

}
