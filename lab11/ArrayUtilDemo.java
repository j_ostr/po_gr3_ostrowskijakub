package com.company.po_gr3_ostrowskijakub.lab11;


import java.time.LocalDate;

public class ArrayUtilDemo {
    public static void main(String[] args)
    {
        Integer[] words = { 15, 51, 1, 614, 13, 6643 };
        System.out.println(ArrayUtil.isSorted(words));
        words = new Integer[]{1, 2, 3, 4, 5, 6};
        System.out.println(ArrayUtil.isSorted(words));
        words = new Integer[]{6,5,7,3,2,11};
        System.out.println(ArrayUtil.isSorted(words));
        LocalDate[] daty = {LocalDate.of(1999,6,22), LocalDate.of(2008,6,3),
                LocalDate.of(1753,12,17),LocalDate.of(1987,4,3)};
        //
        System.out.println(ArrayUtil.binarySearch(words, 5));
        System.out.println(ArrayUtil.binarySearch(daty, LocalDate.of(2008,6,3)));
        ArrayUtil.selectionSort(words);
        ArrayUtil.selectionSort(daty);
        for(int i: words)
        {
            System.out.print(i + " ");
        }
        System.out.println();
        for(LocalDate i: daty)
        {
            System.out.print(i + " ");
        }
        System.out.println();

        words = new Integer[]{6,51,7,3,22,11};
        daty = new LocalDate[]{LocalDate.of(1999,6,22), LocalDate.of(2008,6,3),
                LocalDate.of(1553,12,17),LocalDate.of(1987,4,3)};
        for(int i: words)
        {
            System.out.print(i + " ");
        }
        System.out.println();
        for(LocalDate i: daty)
        {
            System.out.print(i + " ");
        }
        System.out.println();
        ArrayUtil.mergeSort(words, 0, words.length-1);
        ArrayUtil.mergeSort(daty, 0, daty.length-1);

        System.out.println();
        for(int i: words)
        {
            System.out.print(i + " ");
        }
        System.out.println();
        for(LocalDate i: daty)
        {
            System.out.print(i + " ");
        }
    }
}