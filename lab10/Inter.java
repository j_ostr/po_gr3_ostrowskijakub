package com.company.po_gr3_ostrowskijakub.lab10;


import java.time.LocalDate;

public interface Inter extends Comparable, Cloneable{

    String toString();
    boolean equals(Osoba o);
}
