package com.company.po_gr3_ostrowskijakub.lab10;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class TestOsoba {
    public static void main(String[] args)
    {
        ArrayList<Osoba> osoby = new ArrayList<>();
        osoby.add(new Osoba("Nosek", "2001-07-03"));
        osoby.add(new Osoba("Ostrowsi", "2001-03-04"));
        osoby.add(new Osoba("Obrycki", "2001-04-11"));
        osoby.add(new Osoba("Piotrowski", "2001-04-11"));
        osoby.add(new Osoba("Ostrowski", "2001-12-03"));
        for(Osoba o: osoby)
        {
            System.out.println(o.toString());
        }
        System.out.print("\n\n");
        Collections.sort(osoby);
        for(Osoba o: osoby)
        {
            System.out.println(o.toString());
        }
    }

}
