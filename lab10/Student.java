package com.company.po_gr3_ostrowskijakub.lab10;


public class Student extends Osoba {
    public Student(String nazwisko, String data, double sredniaOcen) {
        super(nazwisko, data);
        this.sredniaOcen = sredniaOcen;
    }

    private double sredniaOcen;

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    @Override
    public String toString()
    {
        return String.format("Osoba [%s, %s, %s]", getNazwisko(), getDataUrodznia().toString(), getSredniaOcen());
    }

    @Override
    public int compareTo(Object o) {
        Student os = (Student) o;
        if(this.equals(os))
        {
            return 0;
        } else
        {
            if(this.getNazwisko().equals(os.getNazwisko()))
            {
                return -3;
            }
            if(this.getDataUrodznia().equals(os.getDataUrodznia()))
            {
                return -2;
            }
            if(this.getSredniaOcen() == os.getSredniaOcen())
            {
                return -1;
            }
        }
        return 0;
    }
}
