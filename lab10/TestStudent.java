package com.company.po_gr3_ostrowskijakub.lab10;


import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String[] args)
    {
        ArrayList<Student> osoby = new ArrayList<>();
        osoby.add(new Student("Nosek", "2001-07-03", 5));
        osoby.add(new Student("Ostowski", "2001-03-04", 4.3));
        osoby.add(new Student("Obrycki", "2001-04-11", 4.1));
        osoby.add(new Student("Piotrowski", "2001-04-11", 3.1));
        osoby.add(new Student("Ostrowski", "2001-12-03", 3.2));
        for(Osoba o: osoby)
        {
            System.out.println(o.toString());
        }
        System.out.print("\n\n");
        Collections.sort(osoby);
        for(Osoba o: osoby)
        {
            System.out.println(o.toString());
        }
    }

}
