package com.company.po_gr3_ostrowskijakub.lab10;


import java.time.LocalDate;

public class Osoba implements Inter{

    public Osoba(String nazwisko, String data)
    {
        this.nazwisko = nazwisko;
        this.dataUrodznia = LocalDate.parse(data);
    }

    private String nazwisko;
    private LocalDate dataUrodznia;

    public String getNazwisko() { return nazwisko; }

    public LocalDate getDataUrodznia() { return dataUrodznia; }

    @Override
    public int compareTo(Object o) {
        Osoba os = (Osoba) o;
        if(this.equals(os))
        {
            return 0;
        } else
        {
            if(this.getNazwisko().equals(os.getNazwisko()))
            {
                return -2;
            }
            if(this.getDataUrodznia().equals(os.getDataUrodznia()))
            {
                return -1;
            }
        }
        return 0;
    }

    @Override
    public String toString()
    {
        return String.format("Osoba [%s, %s]", getNazwisko(), getDataUrodznia().toString());
    }
    @Override
    public boolean equals(Osoba o) {
        if (!this.nazwisko.equals(o.getNazwisko())) {
            return false;
        } else return this.dataUrodznia.equals(o.getDataUrodznia());
    }
}
