package com.company.po_gr3_ostrowskijakub.lab10;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Zadanie3 {
    public static void main(String[] args)
    {
        String sciezka = "src/file.txt";
        readFromFile(sciezka);
    }

    public static void readFromFile(String sciezka)
    {
        File file = new File(sciezka);
        ArrayList<String> lista = new ArrayList<>();
        try
        {
            String s;
            BufferedReader br = new BufferedReader(new FileReader(file));
            while((s = br.readLine()) != null)
            {
                lista.add(s);
            }
        } catch(Exception e)
        {
            System.out.println("Brak pliku");
        }
        Collections.sort(lista);
        for(String s : lista)
        {
            System.out.print(s + "\n");
        }
    }

}
